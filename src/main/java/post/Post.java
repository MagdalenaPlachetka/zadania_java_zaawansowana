package post;

import java.util.Arrays;
import java.util.Scanner;

public class Post {

    private double income;
    private final Letter[] letterArray = new Letter[10];
    Parcel parcel = new Parcel();


    public double sendParcel(Parcel parcel, double paid) {
        double rest = 0;
        if (parcel.parcelPrice() <= paid) {
            parcel.setStatus("NADANA");
            rest = paid - parcel.parcelPrice();
            postIncome(parcel.parcelPrice());
            return rest;
        }
        return paid;
    }

    public double postIncome(double income) {
        this.income = this.income + income;
        return this.income;
    }

    public Parcel createParcel() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Sender:");
        String sender = scan.nextLine();

        System.out.println("Receiver:");
        String receiver = scan.nextLine();

        System.out.println("Weight");
        double weight = scan.nextDouble();

        System.out.println("Priority:");
        boolean priority = scan.nextBoolean();

        return new Parcel(sender, receiver, weight, priority);
    }

    public void sendLetter(Letter letter, double paid) {
        if (letter.letterPrice() <= paid) {
            for (int i = 0; i < letterArray.length; i++) {
                if (letterArray[i] != null) {
                    letterArray[i] = letter;
                } else {
                    System.out.println("Przepraszamy, poczta jest w stanie wysyłać tylko 10 listów dziennie, i co nam zrobisz?");
                }
            }
        }
    }

    public void sendPostman() {
        for (Letter letter : letterArray) {
            letter.setStatus("WYSLANY");
        }
    }

    @Override
    public String toString() {
        return "Post{" +
                "income=" + income +
                ", letterArray=" + Arrays.toString(letterArray) +
                '}';
    }
}
