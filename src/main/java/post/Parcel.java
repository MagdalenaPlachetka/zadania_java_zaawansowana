package post;

import java.util.Random;

public class Parcel {

    private final String sender;
    private final String receiver;
    private final double weight;
    private final boolean priority;
    private String status = "utworzona";

    public void setStatus(String status) {
        this.status = status;
    }

    public Parcel() {
        String[] tableOfNames = {"Jan", "Olga", "Karolina"};
        String[] secondTableOfNames = {"Paulina", "Ola", "Tomasz"};
        Random random = new Random();
        int randomNumber = random.nextInt(2) + 1;
        this.sender = tableOfNames[randomNumber];
        this.receiver = secondTableOfNames[randomNumber];
        this.weight = random.nextDouble();
        this.priority = random.nextBoolean();
    }

    public Parcel(String sender, String receiver, double weight, boolean priority) {
        if (sender == null || sender.isEmpty()) {
            throw new RuntimeException("Bledna wartosc nadawcy");
        }
        if (receiver == null || receiver.isEmpty()) {
            throw new RuntimeException("Bledna wartosc odbiorcy");
        }
        if (weight < 1 || weight > 100_000) {
            throw new RuntimeException("Bledna waga");
        }
        this.sender = sender;
        this.receiver = receiver;
        this.weight = weight;
        this.priority = priority;
    }

    public double parcelPrice() {
        int costOfSmallParcel = 5;
        int costOfMiddleParcel = 8;
        int costOfBigParcel = 12;
        if (weight > 0 && weight <= 0.5) {
            if (this.priority) {
                return (costOfSmallParcel * 0.10) + costOfSmallParcel;
            }
            return costOfSmallParcel;
        }
        if (weight > 0.5 && weight <= 1) {
            if (this.priority) {
                return (costOfMiddleParcel * 0.10) + costOfMiddleParcel;
            }
            return costOfMiddleParcel;
        }

        if (weight > 1 && weight <= 2) {
            if (this.priority) {
                return (costOfBigParcel * 0.10) + costOfBigParcel;
            }
            return costOfBigParcel;
        }
        if (weight > 2) {
            double additionalWeight = weight - 2;
            if (this.priority) {
                return (costOfBigParcel + additionalWeight) * 0.10 + costOfBigParcel;
            }
            return costOfBigParcel + additionalWeight;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public String toString() {
        return "Parcel{" +
                "sender='" + sender + '\'' +
                ", receiver='" + receiver + '\'' +
                ", weight=" + weight +
                ", priority=" + priority +
                '}';
    }
}

