package post;

public class Letter {

    private boolean priority;
    private String status = "nadany";
    private Address addressSender;
    private Address addressReceiver;

    public Letter() {
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Letter(Address addressSender, Address addressReceiver) {
        this.addressSender = addressSender;
        this.addressReceiver = addressReceiver;
    }

    public double letterPrice() {
        if (priority) {
            return 8.5;
        } else {
            return 6;
        }
    }

}

