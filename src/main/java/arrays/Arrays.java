package arrays;

import java.util.ArrayList;
import java.util.List;

public class Arrays {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(java.util.Arrays.asList("Magda", "Karolina", "Paulina", "Piotr", "Julia"));
        System.out.println(list);
        System.out.println(list.get(0));
        list.set(0, "Marcin");
        list.remove(2);

        List<String> secondList = new ArrayList<>(java.util.Arrays.asList("Jan", "Maria"));
        secondList.addAll(list);

        for (String names : secondList) {
            System.out.println(names.toUpperCase());
        }

    }

}
