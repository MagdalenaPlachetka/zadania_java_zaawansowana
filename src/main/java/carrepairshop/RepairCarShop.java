package carrepairshop;

public class RepairCarShop {

    private Car car;

    public RepairCarShop(Car car) {
        this.car = car;
    }

    public int repairFlatTire() {
        int quantity = 0;
        for (Wheel wheel : car.getWheelTable()) {
            if (wheel.isFlat()) {
                wheel.setFlat(false);
                quantity++;
            }
        }
        return quantity;
    }
}
