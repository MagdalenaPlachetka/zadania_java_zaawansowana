package carrepairshop;

public class Receipt {

    private String serviceType;
    private int quantity;
    private int price;

    public Receipt(String serviceType, int quantity, int price) {
        this.serviceType = serviceType;
        this.quantity = quantity;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Paragon{" +
                "Rodzaj Usługi='" + serviceType + '\'' +
                ", Ilość =" + quantity +
                ", Cena =" + price +
                '}';
    }
}
