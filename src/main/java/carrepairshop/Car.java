package carrepairshop;

import java.util.Random;

public class Car {
    private Wheel[] wheelTable;

    public Car(Wheel[] wheelTable) {
        this.wheelTable = wheelTable;
    }

    public void setFlatTire() {
        Random random = new Random();
        int randomNumber = random.nextInt(3) + 1;
        wheelTable[randomNumber].setFlat(true);
    }

    public Wheel[] getWheelTable() {
        return wheelTable;
    }
}
