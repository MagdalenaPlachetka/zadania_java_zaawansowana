package carrepairshop;

public class Main {
    public static void main(String[] args) {
        Wheel wheel1 = new Wheel(4, false);
        Wheel wheel2 = new Wheel(3, false);
        Wheel wheel3 = new Wheel(4, false);
        Wheel wheel4 = new Wheel(2, false);

        Wheel[] wheelTable = new Wheel[4];
        wheelTable[0] = wheel1;
        wheelTable[1] = wheel2;
        wheelTable[2] = wheel3;
        wheelTable[3] = wheel4;

        Car car = new Car(wheelTable);
        car.setFlatTire();

        RepairCarShop repairCarShop = new RepairCarShop(car);
        int quantity = repairCarShop.repairFlatTire();

        Receipt receipt = new Receipt("Naprawa opon", quantity, 200);
        System.out.println(receipt);
    }
}
