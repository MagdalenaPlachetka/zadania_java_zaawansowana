package carrepairshop;

public class Wheel {

    private int pressure;
    private boolean isFlat;

    public Wheel(int pressure, boolean isFlat) {
        this.pressure = pressure;
        this.isFlat = isFlat;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public boolean isFlat() {
        return isFlat;
    }

    public void setFlat(boolean flat) {
        isFlat = flat;
    }
}
